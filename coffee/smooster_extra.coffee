# Version: 1.5

class SmoosterPlugin
  constructor: (name, url, data_attribute) ->
    @name = name
    @url = url
    @data_attribute = data_attribute
    @is_loaded = false

class InPlaceEditor
  constructor: (for_element, class_name="", all_editable="true")  ->
    @_content = $(for_element).html()

    @modal_container = $('<div></div>', {'style': 'position: fixed; z-index: 10000; border: 1px solid grey; border-radius: 4px; background-color: #fbfafc; width: 40vw; min-height: 40vh; padding: 40px 10px 10px 10px;', 'class': 'smo-extra-editors '+class_name})
    @modal_container_close = $('<div></div>', {'style': 'position: absolute; top: 0px; right: 10px; color: grey; text-align: center; cursor: pointer; font-size: 30px;', 'class': 'smo-extra-editors'}).html("<span style=\"font-size: 14px; line-height:30px; vertical-align: top;\">zum speichern - schließen</span> <i class='fa fa-times-circle'></i>")
    @modal_container_editable_region = $('<div></div>', {'contenteditable': all_editable, 'style': 'height: 50vh; overflow: auto;'}).html(@_content)
    @modal_container.html(@_content)

    @modal_container.css
      'top': 'calc(50vh - 20vh)',
      'left': 'calc(50vw - 20vw)'
    @modal_container.data('for_element', for_element)
    @modal_container.data('content', @_content)
    @modal_container.html(@modal_container_close).append(@modal_container_editable_region)

    @modal_container_close.click (e) =>
      e.preventDefault()
      $(for_element).html(@modal_container_editable_region.html())
      @destroy()

    $('body').append(@modal_container)

  destroy: ->
    @modal_container.remove()

  @destroy_all: ->
    $('.smo-extra-editors').remove()


class Button
  constructor: (name, faIcon=nil) ->
    @_name = name
    @_fa_icon = faIcon
    @element = $('<a href="#"><i class="fa ' + @_fa_icon + '"></i></a>')

  click: (callback_function) ->
    return unless typeof callback_function == "function"
    @element.click(callback_function)

class Toolbar
  constructor: (for_element, buttons_list=[], class_name="", position_vertical="top", position_horizontal="left") ->
    #@todo add a check if buttons_list is an array

    # check if child elements have already a toolbar - if so, add them
    if $(for_element).find('[data-smo-plugin-has-toolbar]').length > 0
      $(for_element).find('[data-smo-plugin-has-toolbar]').each (index, value) ->
        sub_toolbar = $(value).data('smo-toolbar')
        unless typeof sub_toolbar == "undefined"
          buttons_list = buttons_list.concat(sub_toolbar.get_buttons_list())
          sub_toolbar.destroy()

    @_buttons_list = buttons_list

    btn_container = $('<li></li>', {'style': 'margin-left: 0px; padding: 8px;'})
    buttons = $('<ul></ul>', {'style': 'list-style-type: none; margin: 0px; padding: 0px;'})

    $(buttons_list).each (index, value) ->
      value.element.css
        'color': 'black'
      buttons.append(btn_container.clone().html($(value.element).clone(true,true)))


    toolbar = $('<div></div>', {'style': 'position: absolute; z-index: 10000; border: 1px solid grey; border-radius: 4px; background-color: #fbfafc;', 'class': 'smo-extra-toolbars '+class_name})
    toolbar.append(buttons)

    toolbar_left = '0px'
    toolbar_left = $(for_element).offset().left - toolbar.width() if $(for_element).offset().left - toolbar.width() > 0

    toolbar.css(position_vertical, $(for_element).offset()[position_vertical])
    toolbar.css(position_horizontal, toolbar_left)

    toolbar.data('for_element', for_element)
    $('body').append(toolbar)

    toolbar.hide()

    $(for_element).hover (->
      $('.smo-extra-toolbars').stop(true, true)
      $('.smo-extra-toolbars').hide()
      toolbar.fadeIn()
      return
    ), ->
      $('.smo-extra-toolbars').stop(true, true)
      toolbar.delay(750).fadeOut()
      return

    toolbar.hover (->
      $('.smo-extra-toolbars').stop(true, true)
      return
    ), ->
      $('.smo-extra-toolbars').stop(true, true)
      toolbar.delay(750).fadeOut()
      return

    $(for_element).attr('data-smo-plugin-has-toolbar','true')
    $(for_element).data('smo-toolbar',@)

    @_toolbar = toolbar
    return toolbar

  get_buttons_list: ->
    return @_buttons_list

  destroy: ->
    @_toolbar.remove()

  @destroy_all: ->
    $('.smo-extra-toolbars').remove()


class UiHelper
  constructor: () ->
    # nothing yet
    @_toolbars = []

  create_inplace_editor: (for_element, class_name, all_editable="true") ->
    new InPlaceEditor(for_element, class_name, all_editable)

  create_button: (name, faIcon=nil) ->
    new Button(name, faIcon)

  create_toolbar: (for_element, buttons_list=[], class_name="", position_vertical="top", position_horizontal="left") ->
    if for_element.length > 1
      toolbars = []
      $(for_element).each (index, a_element) =>
        toolbars.push(new Toolbar(a_element, buttons_list, class_name, position_vertical, position_horizontal))
      @_toolbars = @_toolbars.concat(toolbars)
      @trigger("smo:after_toolbar_added")
      return toolbars
    else
      toolbar = new Toolbar(for_element, buttons_list, class_name, position_vertical, position_horizontal)
      @_toolbars.push(toolbar)
      @trigger("smo:after_toolbar_added",toolbar)
      return toolbar

  get_toolbars: ->
    return @_toolbars

  destroy_toolbars: ->
    @destroy_gui();

  destroy_gui: ->
    Toolbar.destroy_all()
    InPlaceEditor.destroy_all()

  on: (callback, callback_function) ->
    jQuery(window).on(callback, callback_function)

  trigger: (callback, params) ->
    jQuery(window).trigger(callback, params)

class SmoosterEditor
  constructor: () ->
    #empty, use if needed

  on: (callback, callback_function) ->
    return unless typeof Mercury == "object"
    switch callback
      when "before_save"
        Mercury.on "before_save", (event) =>
          callback_function()
      when "after_save"
        Mercury.on "saved", (event) =>
          callback_function()
      when "editor_change"
        Mercury.on 'region:update', (event) =>
          callback_function()
      when "image_inserted"
        Mercury.on 'image_inserted', (event) =>
          callback_function()
      when "editor_ready"
        $(window).bind 'mercury:ready', ->
          callback_function()
      else
        @debugMsg("Callback is not defined!")

  edit_image: (image_element) ->
    current_region = window.parent.mercuryInstance.regions.filter (el) ->
      return el.name == $(image_element).closest('[data-mercury]').attr('id')

    Mercury.region = current_region[0]
    Mercury.region.getSelection().selectNode $(image_element).get(0)
    Mercury.region.focus()
    Mercury.trigger('button', {action: 'insertMedia'})


class SmoosterExtra
  api_version: 1.5
  plugin_registry: []
  debug: true

  constructor: () ->
    #empty, use if needed

  debugMsg: (msg) ->
    return unless @debug
    console.log(msg)

  loadFontAwesome: ->
    if $("body").size() > 0 && !window.fontawesomeloaded
      window.fontawesomeloaded = true
      if document.createStyleSheet
        document.createStyleSheet "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"
      else
        $("head").append $("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' type='text/css' media='screen' />")

  add_plugin: (plugin) ->
    @plugin_registry.push plugin
    @debugMsg @plugin_registry

  ui_helper: new UiHelper

  editor: new SmoosterEditor

  load_registry: () ->
    $(@plugin_registry).each (index, plugin) =>

      return if document.querySelectorAll("[#{plugin.data_attribute}]").length == 0 || plugin.is_loaded == true

      @debugMsg "loading... #{plugin.name} former status #{plugin.is_loaded}"
      plugin.is_loaded = true
      js = document.createElement('script')
      js.type = 'text/javascript'
      js.src = plugin.url
      document.body.appendChild js

      max_timeout = 6000
      time_sum = 0
      smooster_extra_Loaded = setInterval((=>
        if typeof window[plugin.name] == "function"
          clearInterval smooster_extra_Loaded
          window[plugin.name].init()
        else
          time_sum += 10
          @debugMsg "failing to load #{plugin.name} because of a timeout after #{time_sum}" if time_sum >= max_timeout
          clearInterval smooster_extra_Loaded if time_sum >= max_timeout
        return
      ), 10)


  @create_plugin: (plugin_name, plugin_url, plugin_attribute) ->
    new SmoosterPlugin(plugin_name, plugin_url, plugin_attribute)

  @add_external_plugin: (plugin_name, plugin_url, plugin_attribute) ->
    max_timeout = 6000
    time_sum = 0
    @external_plugins or (@external_plugins = [])

    @external_plugins.push new SmoosterPlugin(plugin_name, plugin_url, plugin_attribute)

    return if @smooster_add_plugin
    @smooster_add_plugin = setInterval((=>
      if typeof window.SmoosterExtra['instance'] == 'object'
          clearInterval @smooster_add_plugin
          #new_plugin = window.SmoosterExtra.create_plugin('SmoosterPluginTeam', 'http://assets.smooster.me/55e57df862273d609f0002a4/js/plugin_team_admin.js', 'data-smo-plugin-team')
          $(@external_plugins).each (index, value) ->
              window.SmoosterExtra.instance.add_plugin value
          window.SmoosterExtra.instance.load_registry()
      else
        time_sum += 10
        if time_sum >= max_timeout
          clearInterval @smooster_add_plugin
      return
    ), 10)

  @init: ->
    @instance = new SmoosterExtra

    @instance.loadFontAwesome()

    #@instance.add_plugin new SmoosterPlugin("SmoosterPluginDummy", "js/smooster_extra_plugin_boilerplate.js", "data-smo-plugin-dummy")
    @instance.add_plugin new SmoosterPlugin("SmoosterPluginGlide", "http://code.smooster.com/plugins/glide-admin/v2/main.js", "data-smo-plugin-glide")

    @instance.editor.on "before_save", ->
      $('.smo-extra-plugins').remove()

    @instance.load_registry()

@SmoosterExtra or (@SmoosterExtra = SmoosterExtra)


everythingLoaded = setInterval((->
  if /loaded|complete/.test(document.readyState)
    clearInterval everythingLoaded
    SmoosterExtra.init()
  return
), 10)
