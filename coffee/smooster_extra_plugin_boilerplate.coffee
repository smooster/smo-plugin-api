# add Plugin to registry with @add_plugin new SmoosterPlugin([PLUGIN_CLASS_NAME], [PLUGIN_URL], [PLUGIN_INITIATE_HTML_DATA_ATTRIBUTE])

class SmoosterPluginDummy
  smooster_extra: new Object

  constructor: () ->
    #empty, use if needed

  load_ui: () ->
    ui_helper = @smooster_extra.ui_helper
    btn_edit = ui_helper.create_button("copy","fa-copy")
    btn_edit.click (e) ->
      e.preventDefault
      console.log "Deleted all toolbars"
      ui_helper.destroy_toolbars()
    ui_helper.create_toolbar($('[data-smo-plugin-dummy]'),[btn_edit])

  @init: ->
    SmoosterExtra.instance.debugMsg "go go!"
    return false unless SmoosterExtra.instance.api_version >= 1
    plugin_instance = new SmoosterPluginDummy
    plugin_instance.smooster_extra = SmoosterExtra.instance

    plugin_instance.load_ui()
    #call your internal PluginMethod to start with what ever fancy your plugin does.

@SmoosterPluginDummy or (@SmoosterPluginDummy = SmoosterPluginDummy)
