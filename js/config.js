// Generated by CoffeeScript 1.9.2
(function() {
  this.SmoosterExtra || (this.SmoosterExtra = {});

  jQuery.extend(this.SmoosterExtra, {
    config: {
      smo_glide_categories_admin: {
        element: $('[data-smo-categories]'),
        list_items: 'li',
        categories: ['kampagnen', 'print', 'corporate', 'digital'],
        after_add: function() {
          return console.log("after add callback");
        }
      }
    },
    debug: false
  });

}).call(this);
